#!/usr/bin/env python3

# https://www.pythonforbeginners.com/basics/python-docstrings
#         This is a full line comment

import blehmodule

# A docstring provides a way of associating documentation with Python modules,
# functions, classes & methods.
# It is defined by including a string constant (between """ lines) as the first
# statement in the object's definition.
# It's actual source code which is like a comment documenting a specific segment
# of code.
# It is accessed by the __doc__ attribute on objects.

# Docstring example:
def my_function():
    """Do nothing, but document it.

    No, really, it doesn't do anything.
    """
    # The 'pass' statement is a null statement. Useful when you don't write the
    # implementation of a function but you want to do so in future.
    pass

# Since Python 3.x, printing values changed from being a distinct statement
# (i.e. "print my_function.__doc__") to being an ordinary function call. Python
# 2 code examples on this are therefore, now wrong.
print("print blehmodule docstring")
help(blehmodule)
print("print myfunction.__doc__")
print(my_function.__doc__)